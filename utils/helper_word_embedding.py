import numpy as np
from sentence_transformers import SentenceTransformer
from utils.helper_singleton import Singleton


class WordEmbedding(Singleton):

    def __init__(self) -> None:
        self.cache = dict()
        self.cache_sim = dict()
        self.model = SentenceTransformer(
            'sentence-transformers/bert-base-nli-mean-tokens', device='cpu')

    def get_feature(self, text, array=True):
        if text in self.cache:
            return self.cache.get(text)
        output = self.model.encode([text]).reshape(-1,)

        self.cache[text] = output
        return output

    def sim(self, vec1, vec2):
        return np.dot(vec1, vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2))

    def sim_of_two_text(self,  text1, text2):
        key = f'{text1}$$${text2}'
        if key in self.cache_sim:
            return self.cache_sim[key]

        f1, f2 = self.get_feature(text1), self.get_feature(text2)
        res = self.sim(f1, f2)
        self.cache_sim[key] = res
        return res

    def get_rank_list(self, keyword, tags):
        """
        rank_list:
         [
             [tag , sim with keyword],
             [tag , sim with keyword],...
         ]
        """
        rank_list = []
        for i in range(len(tags)):
            rank_list.append([tags[i], self.sim_of_two_text(keyword, tags[i])])
        rank_list = sorted(rank_list,  key =  lambda x: -x[1])
        return rank_list

    def get_top_1_word(self, keyword, tags):
        """
        first element of rank_list
        """
        rank_list = self.get_rank_list(keyword, tags)
        return rank_list[0]
    
    def init_dict(self, tags):

        print("[WE] init existed tags to feature extractor")
        for tag in tags:
            if tag is not None and tag!='':
                self.get_feature(tag)

        print("[WE] init done")
 