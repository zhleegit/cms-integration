### import os
import pickle
import os
from os.path import join as pj, exists

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras import layers, models
from datetime import datetime
from utils.ml_base import MLBase
from utils.helper_word_embedding import WordEmbedding
DATETIME_STR=datetime.now().strftime('%Y-%m-%d-%H%M')

"""
model prediction
 - 0: female , f
 - 1: male , m
"""

class MLNameGenderClassifier(MLBase):
    def __init__(self, model_id = None) -> None:
        super().__init__()
        """folder structure
        ml_data/data_name_gender
        ├── cache (cache of trainable dataset)
        ├── data.csv (whole data)
        ├── model_dir
        │   └─── [datetime.model]
        ├── raw (raw source)
        │   ├── female.txt
        │   └── male.txt
        ├── test.csv (spliitted from whole)
        └── train.csv (splitted from whole)
        """
        ## structural variable
        # root 
        self.data_folder = pj(self.data_folder, "data_name_gender")

        # Level 1 sub
        self.cache = pj(self.data_folder , 'cache')
        self.train_fn = pj(self.data_folder, "train.csv")
        self.test_fn = pj(self.data_folder, "test.csv")
        self.model_dir = pj(self.data_folder , 'model_dir')

        # Level 2 sub
        str_datetime = DATETIME_STR
        if not model_id:
            self.model_id = pj(self.model_dir,  str_datetime)
        else:
            self.model_id = pj(self.model_dir , model_id)
        self.model_fn = self.model_id+'.model'
        self.fig_loss_fn = self.model_id+'_loss.svg'
        self.fig_acc_fn = self.model_id+'_acc.svg'

        ## functional variable
        self.class_to_idx = {'m':1 , 'f': 0 }
        self.idx_to_class = dict([(v, k) for k, v in self.class_to_idx.items()])
        self.N_class = len(set(self.class_to_idx.values()))
        self.WE = WordEmbedding()
        self.train,  self.test =  None, None
        self.train_raw , self.test_raw =  None, None
        self.model = None
        self.history = None

        ## particular variable
        # cached result if user manually sets
        # check user_dict before prediction
        self.user_dict = {}
        self.user_dict_fn = self.model_id+'.udict'

    def load_pair(self, overwrite_cache=False):
        self.train_raw = pd.read_csv(self.train_fn, encoding="utf-8")
        self.test_raw = pd.read_csv(self.test_fn, encoding="utf-8")

        # preprocessing
        ## data augmentation
        tmp_df = pd.DataFrame(
            [
                ["boy", "m"],
                ["male", "m"],
                ["girl", "f"],
                ["female", "f"],
                ["woman", "f"],
                ["man", "m"],
            ],
            columns=self.train_raw.columns,
        )
        self.train_raw = self.train_raw.append(tmp_df, ignore_index=True)
        # preprocessing end
        if exists(self.cache) and not overwrite_cache:
            try:
                with open(self.cache, "rb") as f:
                    self.train, self.test = pickle.load(f)
                return
            except:
                print(f"Error happens when load cache from {self.cache}")
        self.train = self.train_raw.copy()
        self.test = self.test_raw.copy()
        self.train["y"] = self.train_raw["label"].apply(lambda x: self.class_to_idx[x])
        self.train["x"] = self.train_raw["data"].apply(
            lambda x: self.WE.get_feature(str(x))
        )
        self.test["y"] = self.test_raw["label"].apply(lambda x: self.class_to_idx[x])
        self.test["x"] = self.test_raw["data"].apply(
            lambda x: self.WE.get_feature(str(x))
        )
        with open(self.cache, "wb") as f:
            pickle.dump([self.train, self.test], f)
            print(f"cache saved in {self.cache}")

    def init_trained_model(self, epochs=100, train_test_swap=False, patience=10):
        self.model = models.Sequential()
        self.model.add(
            layers.Dense(10, activation="tanh", name="layer1", input_shape=(768,))
        )
        self.model.add(
            layers.Dense(self.N_class, activation="softmax", name="out_layer")
        )
        self.model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"],
        )
        if not train_test_swap:
            data_x, data_y = np.array(self.train["x"].tolist()),\
                            np.array(self.train["y"].tolist())

            data_x_val, data_y_val =np.array(self.test["x"].tolist()),\
                                    np.array(self.test["y"].tolist())
        else:
            data_x, data_y = np.array(self.test["x"].tolist()),\
                            np.array(self.test["y"].tolist())

            data_x_val, data_y_val = np.array(self.train["x"].tolist()),\
                                    np.array(self.train["y"].tolist())

        earlyStopping = tf.keras.callbacks.EarlyStopping(
            monitor="val_loss",
            min_delta=0,
            patience=patience,
            verbose=0,
            mode="auto",
            baseline=None,
            restore_best_weights=True,
        )
        self.history = self.model.fit(
            data_x,
            data_y,
            epochs=epochs,
            validation_data=(data_x_val, data_y_val),
            verbose=2,
            callbacks=[earlyStopping],
        )

        loss, acc = self.model.evaluate(
            np.array(self.train["x"].tolist()),
            np.array(self.train["y"].tolist()),
        )
        loss_val, acc_val = self.model.evaluate(
            np.array(self.test["x"].tolist()),
            np.array(self.test["y"].tolist()),
        )
        print(f"=== train ===\nloss:{loss}\nacc:{acc}")
        print(f"=== test ===\nloss:{loss_val}\nacc:{acc_val}")

        return 

    def save_model(self):
        assert self.model is not None, "model is None"
        self.model.save(self.model_fn)
        with open(self.user_dict_fn, 'wb' ) as f:
            pickle.dump(self.user_dict , f)
        # summarize history for accuracy
        history = self.history
        plt.clf()
        plt.plot(history.history["accuracy"])
        plt.plot(history.history["val_accuracy"])
        plt.title("model accuracy")
        plt.ylabel("accuracy")
        plt.xlabel("epoch")
        plt.legend(["train", "test"], loc="upper left")
        plt.savefig(self.fig_acc_fn, pad_inches = 0.8 , bbox_inches = 'tight')
        print(f'save fig {self.fig_acc_fn}')

        # summarize history for loss
        plt.clf()
        plt.plot(history.history["loss"])
        plt.plot(history.history["val_loss"])
        plt.title("model loss")
        plt.ylabel("loss")
        plt.xlabel("epoch")
        plt.legend(["train", "test"], loc="upper left")
        plt.savefig(self.fig_loss_fn , pad_inches=0.8 , bbox_inches='tight')
        print(f'save fig {self.fig_loss_fn}')

    def load_model(self):
        self.model = tf.keras.models.load_model(self.model_fn)
        with open(self.user_dict_fn , 'rb') as f:
            self.user_dict = pickle.load(f)


    def predict(self, input_name: str, user_dict = False):

        
        if user_dict and (input_name.lower() in self.user_dict):
            print('predict from user_dict')
            return self.user_dict.get(input_name.lower())

        assert self.model is not None, "model is None"
        text_feature = np.array([self.WE.get_feature(input_name)])
        pred_prob = self.model.predict(text_feature)
        print("pred_prob: " ,pred_prob)
        pred_class_index = np.argmax(pred_prob, axis=-1)[0]
        print("pred_class_idx: " , pred_class_index)
        pred_class_token = self.idx_to_class.get(pred_class_index)
        return {
            'class': pred_class_token,
            'prob': pred_prob[0][pred_class_index]
        }

    def update_user_dict(self, key , value):
        key_uncase = key.lower()
        assert value in self.class_to_idx.keys() , 'value shall match class_to_idx.keys() when inserting {k:v} to user_dict.'
        self.user_dict[key_uncase] = {'class': value, 'prob': 1.0 }

    
    def remove_user_dict(self, key):
        if key not in self.user_dict:
            print(f'{key} not in user_dict')
            return False
        self.user_dict.pop()
        return True

    def reset_user_dict(self):
        self.user_dict = {}


    def evaluate(self):
        data_x_val, data_y_val =np.array(self.test["x"].tolist()),\
                                np.array(self.test["y"].tolist())
        self.model.evaluate(data_x_val , data_y_val)




## main procedure
def test():
    print('init flow')
    flow = MLNameGenderClassifier()
    print('load pair')
    flow.load_pair(overwrite_cache=False)
    print('init trained model')
    flow.init_trained_model(train_test_swap=False, epochs=3000, patience=100)
    print('insert user_dict')
    pred = flow.predict('Charlie')
    print(f'predict(\'Charlie\') result: {pred}')

    
    pred = flow.predict('molly')
    print(f'predict(\'molly\') result: {pred}')
    
    
    pred = flow.predict('darren')
    print(f'predict(\'darren\') result: {pred}')
    
    
    pred = flow.predict('amy')
    print(f'predict(\'amy\') result: {pred}')
    
    
    pred = flow.predict('lora')
    print(f'predict(\'lora\') result: {pred}')
    

    flow.save_model()
    print('eval')
    flow.evaluate()
    del flow
    import gc
    gc.collect()
    print('init flow again')
    flow = MLNameGenderClassifier()
    print('load pair again')
    flow.load_pair(overwrite_cache=False)
    print('load model')
    flow.load_model()
    print('eval')
    flow.evaluate()
    print('predict from user_dict again')

    pred = flow.predict('Charlie')
    print(f'predict(\'Charlie\') result: {pred}')
    
    
    pred = flow.predict('molly')
    print(f'predict(\'molly\') result: {pred}')
    
    
    pred = flow.predict('darren')
    print(f'predict(\'darren\') result: {pred}')
    
    
    pred = flow.predict('amy')
    print(f'predict(\'amy\') result: {pred}')
    
    
    pred = flow.predict('lora')
    print(f'predict(\'lora\') result: {pred}')
    
    print('done')
    return flow


def usecase():
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    flow = MLNameGenderClassifier(model_id='2022-01-24-0123')
    flow.load_pair()
    flow.load_model()
    flow.evaluate()

    for n in ['charlie', 'amy', 'lisa', 'molly']:
        print(f'{n}\'s gender:')
        print(flow.predict(n))



flow = MLNameGenderClassifier(model_id='2022-03-24-2144')
flow.load_pair()
flow.load_model()
flow.evaluate()

def predict_gender(name_str,return_dict = False):
    class_prob_dict = flow.predict(name_str)
    if not return_dict:
        return class_prob_dict['class']
    return class_prob_dict
# test()
# usecase()