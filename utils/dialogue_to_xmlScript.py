import sys
from os.path import basename
"""
sample file:
scene: In the library
A: What can I help you with today?

B: When does the library close?

A: The library closes at six o'clock.

B: Does it close at that time every day?

A: Not always.

B: Is the library open on Saturdays?

A: Yes.

B: What time do you open and close on Saturday?

A: The hours are from 9 am to 6:30 pm.

B: Okay. Thank you very much.

A: Do you need anything else?

B: No, that's all. Thanks.
"""

"""
usage:
python dialogue_to_xmlScript.py filename
"""

fn = sys.argv[1]

with open(fn ,encoding='utf-8') as f:
    data = f.read()
datalines = [r.strip() for r in data.split('\n') if r.strip() != '']
scene = [r for r in datalines if 'scene:' in r ][0]
datalines = ['\t\t<actor name=\"'+r.split(':')[0]+'\">\n' +
             '\t\t<dialogue-line>\n\t\t' +
             r.split(':')[-1].strip() +
             '\n\t\t</dialogue-line>\n' +
             '\t\t</actor>'
             for r in datalines if 'scene:' not in r]


output = '<act>\n\t<scene>\n\t\t<stage-direction-scene-set>\n\t\t'+scene+'\n\t\t</stage-direction-scene-set>\n' + \
    '\n'.join(datalines) + '\n\t</scene>\n</act>\n'
new_fn = basename(fn) + '.xml'
with open(new_fn, 'w', encoding='utf-8') as f:
    f.write(output)

print(f'write to {new_fn}')
