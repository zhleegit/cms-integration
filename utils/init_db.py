from logging import Logger
import os
import sys
import shutil
import numpy as np
import pandas as pd
from os.path import join as pj
from sqlalchemy import or_, create_engine, Table, MetaData, Column, Integer, TEXT
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import database_exists, create_database

# relative location cannot be changed
def import_config():
    global config
    sys.path.append('..')
    import config as config_instance
    config = config_instance

config = None
import_config()
component_dir = pj('..','cms-data', 'component')
# clean dir before init cms-data/component
if len(os.listdir(component_dir)) > 0:
    r = input(
        '[WARN] cms-data/component has some data, sure to remove them all? [y]')
    if r.lower() != 'y':
        print('[INFO] cms-data/component is not cleaned')
        exit()
    import clean
    print('[INFO] cms-data/component is cleaned')


print('[INFO] start init db')


table_name = config.DB_TABLE
db_user = config.DB_USER
db_url = config.DB_URL
db_name = config.DB_NAME
db_pwd = config.DB_PWD


engine = create_engine(
    f'mariadb+pymysql://{db_user}:{db_pwd}@{db_url}/{db_name}?charset=utf8', echo=True
)


if not database_exists(engine.url):
    create_database(engine.url)


session = sessionmaker(bind=engine)()
Base = declarative_base()


# create table
metadata = MetaData(engine)
comp = Table(table_name,
             metadata,
             Column('id', Integer, primary_key=True),
             Column('src', TEXT),
             Column('tag1', TEXT, nullable=True),
             Column('tag2', TEXT,  nullable=True),
             Column('tag3', TEXT, nullable=True),
             Column('tag4', TEXT, nullable=True),
             Column('tag5', TEXT,  nullable=True),
             Column('tag6', TEXT,  nullable=True),
             Column('tag7', TEXT,  nullable=True),
             Column('tag8', TEXT,  nullable=True),
             )
metadata.create_all(engine)

# declare data model


class Component(Base):
    __tablename__ = table_name
    id = Column(Integer, primary_key=True)
    src = Column(TEXT)
    tag1 = Column(TEXT)
    tag2 = Column(TEXT)
    tag3 = Column(TEXT)
    tag4 = Column(TEXT)
    tag5 = Column(TEXT)
    tag6 = Column(TEXT)
    tag7 = Column(TEXT)
    tag8 = Column(TEXT)

    def __init__(self, src,  tags):
        self.src = src
        for i in range(len(tags)):
            if tags[i] is np.nan:
                continue
            if i == 0:
                self.tag1 = tags[i]
            elif i == 1:
                self.tag2 = tags[i]
            elif i == 2:
                self.tag3 = tags[i]
            elif i == 3:
                self.tag4 = tags[i]
            elif i == 4:
                self.tag5 = tags[i]
            elif i == 5:
                self.tag6 = tags[i]
            elif i == 6:
                self.tag7 = tags[i]
            elif i == 7:
                self.tag8 = tags[i]


# read data from init
tag_src_fn = pj('init_db_data', 'db_init_src_tag.csv')
tag_src_df = pd.read_csv(tag_src_fn)


def to_db_record(record):
    src = record['src']
    comp_fn = src.split('/')[1]

    # relative to app.py
    new_src = pj('cms-data', 'component', comp_fn)

    tags = []
    for col in ['category', 'gender', 'description', 'verb', 'o', 'adv']:

        tag = record.get(col)
        tag = tag_transformer(tag, col)
        tags.append(tag)

    return Component(new_src, tags)


def tag_transformer(tag, col):
    dic = {
        '男生': 'male',
        '女生': 'female',
    }

    if col != 'gender':
        return tag
    return dic.get(tag, tag)


def add_to_cms_data(record):
    src = record['src']
    cat = record['category']

    fn = src.split('/')[1]
    original_src = pj('init_db_data', src)

    if os.path.isdir(original_src) and record.get('category').lower() == 'mouth':
        shutil.copytree(original_src, pj(component_dir, fn))
    else:
        shutil.copy2(original_src, component_dir)


comps = []
for record in tag_src_df.to_dict('records'):
    src = record['src']
    cat = record['category']
    if cat not in {'character', 'mouth', 'background'}:
        # print(f'[INFO] skip record src: {src} of type: {cat}')
        continue

    print(f'[INFO] insert src: {src} of type: {cat}')
    comps.append(to_db_record(record))
    add_to_cms_data(record)

flen = len(os.listdir(component_dir))
rlen = len(comps)
if flen != rlen:
    print(f'flen : {flen} , rlen: {rlen}')
    exit()

session.add_all(comps)


# session.add_all([
#     Component(src=f'zh{i}' , tag1=None) for i in range(1, 20)
# ])

session.commit()
session.close()
