from random import shuffle
from operator import index
import os
import pandas as pd



with open("female.txt", "r", encoding="utf-8") as f:
    fdata = f.readlines()
fdata = fdata[6:]


with open("male.txt", "r", encoding="utf-8") as f:
    mdata = f.readlines()
mdata = mdata[6:]


fdata = [[str(d), "f"] for d in fdata if d != ""]
val_flen = int(len(fdata) * 0.1)


mdata = [[str(d), "m"] for d in mdata if d != ""]
val_mlen = int(len(mdata)* 0.1)


shuffle(fdata)
shuffle(mdata)


val_f, train_f, val_m, train_m = (
    fdata[:val_flen],
    fdata[val_flen:],
    mdata[:val_mlen],
    mdata[val_mlen:],
)

val = val_f + val_m
train = train_f + train_m

shuffle(val)
shuffle(train)

df_train = pd.DataFrame(train, columns=["data", "label"])
df_val = pd.DataFrame(val, columns=["data", "label"])


df_train.to_csv('./../train.csv', encoding='utf-8' , index= False)
df_val.to_csv('./../test.csv', encoding='utf-8' , index= False)

