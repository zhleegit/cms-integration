import logging

logging.basicConfig(
#     filename= pj('logs' , 'tj.log'),
    level=logging.INFO,
    format='%(asctime)s [%(levelname)s] : %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

def printLog(text):
    logging.info(f'====== {text} =====')
