import abc
from os.path import join as pj
import numpy as np
np.set_printoptions(suppress=True)

class MLBase(abc.ABC):

    def __init__(self) -> None:
        super().__init__()
        self.model = None
        self.data_folder = pj('utils','ml_data')
        self.train, self.test = None, None
        self.train_fn , self.test_fn = None, None
        self.train_raw , self.test_raw = None, None
        self.class_map =None
        self.idx_to_class = None
        self.history = None

    @abc.abstractmethod
    def load_pair(self, folder):
        """
        required struct:
        data_[set name]
        ├── test.csv
        └── train.csv
        """
        pass

    @abc.abstractmethod
    def init_trained_model(self):
        pass

    @abc.abstractmethod
    def save_model(self):
        pass

    @abc.abstractmethod
    def load_model(self):
        pass

    @abc.abstractmethod
    def predict(self):
        pass

