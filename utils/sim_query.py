from helper_word_embedding import WordEmbedding


WE = WordEmbedding()

# cache of WE.get_feature
tagmap = dict()


def init_tags(tags):
    global tagmap
    print("[sim query] init tagmap")

    for tag in tags:
        tagmap[tag] = WE.get_feature(tag)


def update_tags(tags):

