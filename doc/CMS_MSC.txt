title CMS (1.2.3)

actor "User" as u
participant "Main" as m
participant "CMS server" as c
participant "MariaDB" as db

u->m:url/[index]
m->u:index.html\n(allow entering CMS or SG)
u->c:click "component management"

==system switch to CMS server==
c->db:"select * from cms.components;"
c->u:cms.html (list component)




group 1.2.3.6 upload
u->c:click "upload component"
c->c:verify file format
c->u:editTag.html
u->c:enter tag (editText area 1~8), submit form
c->c:move file to "cms-data/component" \n(rename)
c->db:insert (src, tags) record
db->c:ok
c->u:ok (with button "back to CMS")
end

group 1.2.3.7 query by tag
u->c:enter keyword to editText area
u-->c:enter another keyword \n(splitted by OR/AND)
u->c:click "search" \n(commit)
c->db:"select * from cms.components where...;"
c->u:cms.html \n(list component by condition,\nshow condition text,\ncount)
end

group 1.2.3.8 list component
==[skip] default view lists all components==

end

group 1.2.3.9 edit tag
u-->c:query by tag
u->c:click "edit tag"
c->u:editTag.html
u->c:enter tag (editText area 1~8), submit form
c->db:modify (src, tags) record
db->c:ok
c->u:ok (with button "back to CMS")
end

group 1.2.3.11 download component
u-->c:query by tag
u->c:click "download"
c->c:search file from "cms-data/component" by src of id
c->u:return file
c->u:cms.html(list component)
end

group 1.2.3.12 delete
u-->c:query by tag
u->c:click "delete"
c->u:popup "sure ?"
u->c:click "sure"
c->c:delete file from "cms-data/component"
c->db:remove (src, tags) record
db->c:ok
c->u:ok (with button "back to CMS")
end

