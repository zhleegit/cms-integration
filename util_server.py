"""
致 接手這東西的小夥伴﹔


這玩意從 idea 到完成經過一年多風風雨雨，
滿是歷史的軌跡與忘記意義的 Dependencies
不建議在未知後果的情況下動它!
開發新功能建議用「疊加法」，
閒到不知道要幹麻才考慮 refactor or re-write
想清楚你要的是什麼

Remember three things:

- "Your time is limited, 
so don't waste it living someone else's life". - Steven Jobs

- Always save a runnable version!

- If it works, don't touch it!

"""


"""
api:

/get_nouns?sentence=...

/get_gender?name_string=...


/read_xml?xml_filename=...
 test-cases:
 http://127.0.0.1:5004/read_xml?xml_filename=test-cases/sample-onepage.xml
 http://127.0.0.1:5004/read_xml?xml_filename=test-cases/sample-twopage.xml

"""

from pprint import pprint
import xml.etree.ElementTree as ET
from utils.Logger import printLog
from utils.ml_name_gender_classifier import predict_gender
from flair.data import Sentence
from flair.models import SequenceTagger
import flair
import config
from flask import Flask, jsonify, request
import torch
import os
import sys
sys.path.append('utils')
if 'test' in sys.argv[-1]:
    config.debug =  True
    config.URL = '127.0.0.1'
    config.update_host()

os.environ['CUDA_VISIBLE_DEVICES'] = ''
flair.device = torch.device('cpu')


app = Flask(__name__)
PORT = config.PORT_UTIL

 
tagger = SequenceTagger.load("flair/pos-english")


def read_xml(fn='test-cases/stage-play-script1.xml'):
    try:
        print(f'util_server receive xml_filename: {fn}')
        tree = ET.parse(fn)

        # one slide contains: [scene, actor, dialogue]
        slides = []

        # seq = []
        actors = set()
        actor_to_id = dict()
        scenes = set()

        scene = None
        slide_info = None

        count_actor = 0
        count_page = 0
        for i, child in enumerate(tree.getroot()[0]):

            if 'actor' in child.tag.lower():
                actor = child.get('name').strip().lower()
                actors.add(actor)
                if actor not in actor_to_id:
                    actor_to_id[actor] = count_actor
                    count_actor += 1

                dialog = child[0].text.strip()
                slide_info = {
                    # 'scene': scene,
                    # 'actor': actor,
                    'speaker_gender': get_gender(actor),
                    'dialogue': dialog,
                    'speaker_id': actor_to_id[actor],
                    'page_id': count_page
                }
                count_page += 1
                slides.append(slide_info)

            if 'scene-set' in child.tag.lower() and scene is None:
                scene = child.text.strip().lower()

        scene = scene if scene is not None else 'default scene'

        return {'success': True,
                'slides': slides,
                'actors': actor_to_id,
                'scenes': scene}
    except:
        printLog('error occurs when parsing xml')
        return {'success': False}



def read_xml_v2(fn='test-cases/stage-play-script-2scene.xml'):
    try:
        print(f'util_server receive xml_filename: {fn}')
        tree = ET.parse(fn)

        # one slide contains: [scene, actor, dialogue]
        slides = []

        # seq = []
        actors = set()
        actor_to_id = dict()
        scenes = set()

        scene = None
        slide_info = None

        count_actor = 0
        count_page = 0
        for i, child in enumerate(tree.getroot()[0]):

            if 'actor' in child.tag.lower():
                actor = child.get('name').strip().lower()
                actors.add(actor)
                if actor not in actor_to_id:
                    actor_to_id[actor] = count_actor
                    count_actor += 1

                dialog = child[0].text.strip()
                slide_info = {
                    # 'scene': scene,
                    # 'actor': actor,
                    'speaker_gender': get_gender(actor),
                    'dialogue': dialog,
                    'speaker_id': actor_to_id[actor],
                    'page_id': count_page
                }
                count_page += 1
                slides.append(slide_info)

            if 'scene-set' in child.tag.lower() and scene is None:
                scene = child.text.strip().lower()

        scene = scene if scene is not None else 'default scene'

        return {'success': True,
                'slides': slides,
                'actors': actor_to_id,
                'scenes': scene}
    except:
        printLog('error occurs when parsing xml')
        return {'success': False}







@app.route('/read_xml')
def api_read_xml():

    slides = request.args.get('xml_filename', '', type=str)
    print(f'slide: {slides}')
    res = read_xml(slides)

    resp = {
        'status': 200,
        'res': res
    }
    if res is None:
        resp['status'] = 404

    pprint(resp)
    return jsonify(resp)
    


@app.route('/read_xml_v2')
def api_read_xml_v2():

    slides = request.args.get('xml_filename', '', type=str)
    print(f'slide: {slides}')
    res = read_xml(slides)

    resp = {
        'status': 200,
        'res': res
    }
    if res is None:
        resp['status'] = 404

    pprint(resp)
    return jsonify(resp)
    


def get_nouns(sentence_raw, return_dict=False):
    """
    get nouns from sentence
    """
    printLog(f'util: get_nouns: receive {sentence_raw}')
    sentence = Sentence(sentence_raw)
    tagger.predict(sentence)

    word_labels = []
    for entity in sentence.get_spans('pos'):
        # print(entity)
        text = entity.text
        label = entity.get_labels()[0].value
        if 'NN' in label:
            word_labels.append({'text': text, 'label': label})

    print('output:')
    print(word_labels)
    if not return_dict:
        return [dic['text'] for dic in word_labels]
    return word_labels


@app.route('/get_nouns')
def api_get_nouns():
    sentence = request.args.get('sentence', '', type=str)
    res = get_nouns(sentence)
    pprint(res)
    return jsonify({'nouns': res})


cache  = {}
def get_gender(name_str):
    """
    return class
    'w'/'m'
    """
    global cache 
    if name_str in cache:
        return cache[name_str]
    dic = predict_gender(name_str, return_dict= True)
    res = {'gender': dic['class'] ,'prob':  str(dic['prob'])}

    cache[name_str] = res
    return res


@app.route('/get_gender')
def api_get_gender():
    name_string = request.args.get('name_string', '', type=str)
    res = get_gender(name_string)
    print(res)
    pprint(res)
    return jsonify(res)


# slides= read_xml()
# for slide_info in slides:
#     print('sentence: ' + slide_info['scene'])
#     print(get_nouns(slide_info['scene']))


def test():
    printLog('test gender classification')
    print('charlie:')
    print(get_gender('Charlie'))
    print('molly:')
    print(get_gender('molly'))
    print('\n\n')

    printLog("test get nouns")
    sent = "On the deck of the ship . The captain and his first mate enters ."
    print(sent + '\n')
    print(get_nouns(sent))
    print('\nn')

    printLog("test read_xml")
    print(read_xml())
    print('\n\n')


# test()

if __name__ == '__main__':
    debug = True if config.debug else  False 
    app.run(debug=debug, port=PORT, host='0.0.0.0')

