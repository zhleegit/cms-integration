
import os
import subprocess
from os import chdir, getcwd, system
from os.path import join as pj
from shutil import rmtree
from sys import platform
from time import sleep
from turtle import position, width

import cv2
from google.cloud import texttospeech
from lxml import etree
from PIL import Image
from pptx import Presentation
from pptx.dml.color import RGBColor
from pptx.util import Inches, Pt

import config

OUTPUT_PATH = config.SG_OUTPUT_PATH
add_first_page = config.SG_ADD_FIRST_PAGE

"""

primary fuctions
- gen_slide(slides[*] , out_fn )

python-pptx usecases
reference:
https://www.geeksforgeeks.org/creating-and-updating-powerpoint-presentations-in-python-using-python-pptx/

"""

layout_id_table = {
    "title and subtitle": 0,
    "title and content": 1,
    "section header": 2,
    "two content": 3,
    "Comparison": 4,
    "Title only": 5,
    "Blank": 6,
    "Content with caption": 7,
    "Pic with caption": 8,
}

layout_id = layout_id_table[config.SG_CONTENT_LAYOUT]
tmp_voice = config.SG_TMP_VOICE
tmp_rhubarb = config.SG_TMP_RHUBARB
tmp_lipsyncgif = config.SG_TMP_LIPSYNCGIF
rhubarb_dir = config.SG_RHUBARB_DIR
rhubarb_exe = config.SG_RHUBARB_EXE
rhubarb_exe = pj('.', rhubarb_exe)


MODEL_SOURCE = os.path.join('datasource', 'cascade.xml')


from threading import Semaphore

sem = Semaphore()
class Slide:
    def __init__(self,
                 page_id=None,
                 text=None,
                 speaker_name=None,
                 actor_srcs=None,
                 background_src=None,
                 speaker_gender=None,
                 speaker_id=0,
                 mouth_dir='mouth_set_1'):

        self.page_id = page_id
        if text is None:
            self.init_test()
        else:
            self.text = text
            self.actor_srcs = actor_srcs
            self.background_src = background_src
            self.speaker_id = speaker_id
            self.speaker_gender = speaker_gender
            if self.actor_srcs is not None:
                self.speaker_src = self.actor_srcs[self.speaker_id]
        self.speaker_name = speaker_name.capitalize()
        # male:     AB D    IJ
        # female:     C EFGH
        male_voice = ['A', 'B', 'D', 'I', 'J']
        female_voice = ['C', 'E', 'F', 'G', 'H']

        if speaker_gender == 'm':
            self.VOICE_NAME = f'en-US-Standard-{male_voice[speaker_id]}'
        else:
            self.VOICE_NAME = f'en-US-Standard-{female_voice[speaker_id]}'
        print(
            f"name: {self.speaker_name} of gender: {self.speaker_gender} in  page: {self.page_id} with voice {self.VOICE_NAME}")
        # assert len(self.text.split(':')) == 2, 'check input text'

        audio_fn_test = f'page_{page_id}.wav'
        audio_src = pj(tmp_voice, hash_filename(audio_fn_test, tmp_voice))

        # audio_src = pj(tmp_voice, f'page_{page_id}.wav')
        self.text_to_speech(text_in=self.text,
                            wav_path=audio_src)
        self.audio_src = audio_src

        phoneme_src = self.gen_phoneme_seq(self.audio_src, page_id)
        times = 0
        while not os.path.exists(phoneme_src) and times < 4:
            times += 1
            sleep(5)
            print(f"==== retry {times} rhubarb to generate {phoneme_src}")
            phoneme_src = self.gen_phoneme_seq(self.audio_src, page_id)

        self.phoneme_src = phoneme_src

        lipsyncgif_fn_test = f'gen_{self.page_id}.gif'
        self.lipsyncgif_src = pj(config.SG_TMP_LIPSYNCGIF,
                                 hash_filename(lipsyncgif_fn_test, config.SG_TMP_LIPSYNCGIF))

        # self.lipsyncgif_src = pj(config.SG_TMP_LIPSYNCGIF,
        #                     f'gen_{self.page_id}.gif')

    def gen_lipsyncgif_with_retry(self):
        # mouth_dir = pj('cms-data', 'component', mouth_dir)
        mouth_dir = self.mouth_dir
        self.gen_lipsyncgif(mouth_dir=mouth_dir,
                            out_src=self.lipsyncgif_src,
                            phoneme_src=self.phoneme_src)
        timeout = 0
        while not os.path.exists(self.lipsyncgif_src):
            timeout += 1
            sleep(0.5)
            print(f'wait for lipsync generation to {self.lipsyncgif_src}')

            if timeout > 6:
                print('fail to generate lipsync gif')
                exit()

    def init_test(self):
        print("[SG INFO] init with test data")
        self.text = 'Amy:test case title'
        self.actor_srcs = ['test-cases/actor1.png',
                           'test-cases/actor2.png']
        self.background_src = 'test-cases/background.jpg'
        self.audio_src = 'test-cases/sample.wav'
        self.speaker_id = 1
        self.speaker_src = self.actor_srcs[self.speaker_id]
        self.speaker_gender = 'female'

    def check_argument(self):
        msg = f'[SG check, page_id={self.page_id}, autio_src={self.audio_src}]'
        tmp = msg
        if not len(self.actor_srcs) > 0:
            msg += 'actor shall > 0'
        elif not os.path.exists(self.background_src):
            msg += 'background not exists'
        elif not os.path.exists(self.audio_src):
            msg += 'audio not exists'

        if msg != tmp:
            print(msg)
            return False

        if self.text == '' or self.text is None:
            print("[SG check] title is none or empty string")

        return True

    def text_to_speech(self, text_in, wav_path):
        """
        Synthesizes speech from the input string of text or ssml.
        Note: ssml must be well-formed according to:
            https://www.w3.org/TR/speech-synthesis/
        """
        # Instantiates a client
        client = texttospeech.TextToSpeechClient()
        # Set the text input to be synthesized
        synthesis_input = texttospeech.SynthesisInput(text=text_in)
        # Build the voice request, select the language code ("en-US") and the ssml
        # voice gender ("neutral")
        voice = texttospeech.VoiceSelectionParams(
            language_code="en-US", name=self.VOICE_NAME, ssml_gender=texttospeech.SsmlVoiceGender.NEUTRAL
        )
        # Select the type of audio file you want returned
        audio_config = texttospeech.AudioConfig(
            audio_encoding=texttospeech.AudioEncoding.LINEAR16
        )
        # Perform the text-to-speech request on the text input with the selected
        # voice parameters and audio file type
        response = client.synthesize_speech(
            input=synthesis_input, voice=voice, audio_config=audio_config
        )
        # The response's audio_content is binary.
        with open(wav_path, "wb") as out:
            # Write the response to the output file.
            out.write(response.audio_content)
            print('\nAudio content written to file "' + wav_path + '"\n')

    def gen_phoneme_seq(self,
                        wav_src,
                        page_id):

        phoneme_fn_test = f'rhu_out_{page_id}.txt'
        phoneme_src = pj(tmp_rhubarb, hash_filename(
            phoneme_fn_test, tmp_rhubarb))
        # phoneme_src = pj(tmp_rhubarb, f'rhu_out_{page_id}')

        # cwd = getcwd()
        # sem.acquire()
        cwd = config.CWD


        # todo: old 
        # chdir(rhubarb_dir)
        # todo:new
        
        rhubarb_exe_path  = pj(rhubarb_dir , rhubarb_exe)
        

        # cmd = ' '.join([rhubarb_exe, pj(cwd, wav_src),
        cmd = ' '.join([rhubarb_exe_path, pj(cwd, wav_src),
                        '-o', pj(cwd, phoneme_src)])
        print('====== rhubarb cmd =====\n'+cmd+'\n=======================')
        # subprocess.run([rhubarb_exe, pj(cwd, wav_src),
        subprocess.run([rhubarb_exe_path, pj(cwd, wav_src),
                        '-o', pj(cwd, phoneme_src)])

        # subprocess.run(cmd.strip().split(' '))
        # subprocess.call(cmd)
        # system(cmd)

        # todo: old 
        # chdir(cwd)
        # todo:new


        # sem.release()
        return phoneme_src

    def gen_lipsyncgif(self,
                       mouth_dir,
                       out_src,
                       phoneme_src,
                       dur=0.01):
        # Read PNG
        src_img_fn = ['A.png', 'B.png', 'C.png',
                      'D.png', 'E.png', 'F.png', 'G.png', 'H.png']
        src_img = []
        mouth_list = []
        gif_list = []

        for fn in src_img_fn:
            img = Image.open(os.path.join(mouth_dir, fn))
            # deal with transparency
            alpha = img.getchannel('A')
            img = img.convert('RGB').convert(
                'P', palette=Image.ADAPTIVE, colors=255)
            mask = Image.eval(alpha, lambda a: 255 if a <= 128 else 0)
            img.paste(255, mask)
            img.info['transparency'] = 255
            src_img.append([fn[0], img])

        # Read mouth.txt
        with open(phoneme_src) as mouth_f:
            for line in mouth_f:
                mouth = line[:-1].split('\t')
                if mouth[1] == 'X':
                    mouth[1] = 'A'
                mouth_list.append([mouth[0], mouth[1]])
            for i in range(0, len(mouth_list)-1):
                mouth_list[i][0] = float(
                    mouth_list[i+1][0]) - float(mouth_list[i][0])
            mouth_list.pop(len(mouth_list)-1)

        # Generate gif list
        for i in mouth_list:
            # Round the duration
            i[0] = int((float(i[0]) + dur/2)//dur)
            for img in src_img:
                if i[1] == img[0]:
                    for j in range(0, i[0]):
                        gif_list.append(img[1])

        gif_list[0].save(out_src, save_all=True,
                         append_images=gif_list[1:], duration=dur*1000, loop=1)
        print('\nGif is written to file "' + out_src + '"')


def gen_slide(slides=[], out_fn='output.pptx', sg_collection=None):
    """
    primary function for SG
    """
    print(f'sg_procedure receive: slides {slides} \n\n output_fn: {out_fn}')

    # clean_tmp_voice()
    prs = Presentation()

    # Creating slide layout
    first_slide_layout = prs.slide_layouts[0]

    if add_first_page:
        prs = create_first_page(prs,
                                title='sample title')

    # 最多放三個人物不擁擠

    if len(slides) == 0:
        print("[INFO] use test slide arguments")
        slides = []
        for i in range(2):
            slide_obj = Slide(page_id=i)
            slides.append(slide_obj)
    prs = create_slide(prs,
                       slides,
                       sg_collection)

    out_ppt_path = pj(OUTPUT_PATH, out_fn)
    print(out_ppt_path)
    prs.save(out_ppt_path)
    clean_tmps(slides)

    return out_ppt_path


def clean_tmps(slides):
    for slide in slides:
        print(f'clean slide id: {slide.page_id}')
        for target in [slide.audio_src , slide.lipsyncgif_src , slide.phoneme_src]:
            if os.path.exists(target):
                print(f"file: {target}")
                os.remove(target)

def create_first_page(prs, title='sample title'):
    """
    rarely used
    """
    slide = prs.slide_layouts[layout_id_table['title and subtitle']]
    slide = prs.slides.add_slide(slide)
    slide.shapes.title.text = title
    return prs


from time import time

hash_count = 0
def hash_filename(fn, target_folder='.'):
    global hash_count
    if '.' not in fn:
        return

    file_type = fn.split('.')[-1]
    fn = str(int(time()))+str(hash_count)
    hash_count+=1
    new_fn = hex(hash(fn))[3:]+f'.{file_type}'

    while (new_fn in os.listdir(target_folder)):
        fn = str(int(time()))
        new_fn = hex(hash(fn))[3:]+f'.{file_type}'
    print("generate hash: {new_fn}")
    return new_fn


def create_slide(prs,
                 slides,
                 sg_collection,
                 font_size_pt=30):
    """
    insert one slide to prs
    """

    total = len(slides)
    for i, slide_obj in enumerate(slides):
        sg_collection.progress_str_config = str(int(100*(i+1)/total))

        slide_obj.mouth_dir = slide_obj.mouth_dirs[slide_obj.speaker_id]
        slide_obj.gen_lipsyncgif_with_retry()

        assert slide_obj.check_argument(), "argument check fail"

        print(f'processing page: {slide_obj.page_id}')

        title = slide_obj.speaker_name+': '+slide_obj.text
        actor_srcs = slide_obj.actor_srcs
        background_src = slide_obj.background_src
        audio_src = slide_obj.audio_src
        lipsyncgif_src = slide_obj.lipsyncgif_src
        speaker_src = slide_obj.actor_srcs[slide_obj.speaker_id]

        slide = prs.slide_layouts[layout_id]
        slide = prs.slides.add_slide(slide)

        # audio
        print(f"add audio src : {audio_src}")
        movie = slide.shapes.add_movie(
            audio_src, 0, 0,  width=Inches(0.7), height=Inches(0.7))
        autoplay_media(movie)

        # banner
        bg_pic = slide.shapes.add_picture(config.SG_BANNER_SRC,
                                          Inches(0.6),
                                          Inches(0.5),
                                          width=Inches(9),
                                          height=Inches(1))

        slide.shapes._spTree.insert(2, bg_pic._element)

        # backgorund
        bg_pic = slide.shapes.add_picture(background_src,
                                          0,
                                          0,
                                          width=prs.slide_width,
                                          height=prs.slide_height)

        slide.shapes._spTree.insert(2, bg_pic._element)

        # dialog text
        slide.shapes.title.text = title
        slide.shapes.title.text_frame.paragraphs[0].font.size = Pt(
            font_size_pt)
        # slide.shapes.title.fill.fore_color.RGB = RGBColor(255, 255, 255)
        # slide.shapes.title.text_frame.paragraphs[0].font.color.rgb = RGBColor(255, 255, 255)

        # actors
        # layout
        #
        #      slide_width
        #  ____________________________________
        # |     |                |             |
        # |lower|  valid width   |upper        |
        # |     |                |             |
        #        first_x          last_x
        LOWER = Pt(36)
        UPPER = Pt(300)
        ACTOR_Y = Pt(190)

        y = ACTOR_Y
        if len(actor_srcs) == 1:
            xs = [LOWER]
        else:
            valid_width = prs.slide_width - LOWER - UPPER
            xs = [LOWER + valid_width*i/(len(actor_srcs)-1)
                  for i in range(len(actor_srcs))]
        speaker_x, speaker_y = xs[slide_obj.speaker_id], y
        speaker_resize_ratio = 1
        for i, actor in enumerate(actor_srcs):
            x = xs[i]
            w_prime, h_prime, resize_ratio = get_img_scale(actor)
            if i == slide_obj.speaker_id:
                speaker_resize_ratio = resize_ratio
            print("actor poistion: ")
            print([x, y, w_prime, h_prime])
            print(f"add actor src : {actor}")
            pic = slide.shapes.add_picture(actor,
                                           x,
                                           y,
                                           width=w_prime,
                                           height=h_prime)

        # gif mouth
        position_info = get_lipsyncgif_position(speaker_src)
        # x, y = Inches(3), Inches(3)
        # w, h = get_img_scale(lipsyncgif_src)
        x, y, w, h = [Pt(s) for s in position_info]

        x *= speaker_resize_ratio
        y *= speaker_resize_ratio
        x += speaker_x
        y += speaker_y
        w *= speaker_resize_ratio
        h *= speaker_resize_ratio
        print(f'mouth at ppt: {x}, {y}\nresize rate: {speaker_resize_ratio}')
        print(f"add lipsyncfig_src: {lipsyncgif_src}")
        pic = slide.shapes.add_picture(lipsyncgif_src,
                                       x,
                                       y,
                                       width=w,
                                       height=h)
    return prs


def autoplay_media(media):
    el_id = xpath(media.element, './/p:cNvPr')[0].attrib['id']
    el_cnt = xpath(
        media.element.getparent().getparent().getparent(),
        './/p:timing//p:video//p:spTgt[@spid="%s"]' % el_id,
    )[0]
    cond = xpath(el_cnt.getparent().getparent(), './/p:cond')[0]
    cond.set('delay', '0')


def xpath(el, query):
    nsmap = {'p': 'http://schemas.openxmlformats.org/presentationml/2006/main'}
    return etree.ElementBase.xpath(el, query, namespaces=nsmap)


def get_lipsyncgif_position(speaker_src):
    smiles = detect(speaker_src)
    miny = 10000
    for smile in smiles:
        if smile[1] < miny:
            (x, y, w, h) = smile
            miny = y
    y = int(y+h/5)
    r = int((h+1)/2*2)
    x = int(x+w/2-r/2)
    print(f'smile pt:  {x},{y},{r},{r}')
    return x, y, r, r


def detect(speaker_src):
    image = cv2.imread(speaker_src)
    detector = cv2.CascadeClassifier(MODEL_SOURCE)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    mins = (image.shape[0] // 120) * 10
    maxs = (image.shape[0] // 40) * 10
    smiles = detector.detectMultiScale(
        gray, scaleFactor=1.2, minNeighbors=50, minSize=(mins, mins), maxSize=(maxs, maxs))
    # ouput the image with mouth rectangle
    if len(smiles) <= 0:
        print("Mouth detect fail.")
        return
    return smiles


def get_img_scale(img_src):
    img = Image.open(img_src)
    w, h = img.size[0], img.size[1]
    # h_out = Inches(5)
    # w_out = Inches((w/h)*5)
    h_out = Pt(inch_to_pt(5))
    w_out = Pt(inch_to_pt((w/h)*5))
    # x_inch, y_inch = 1, 1
    resize_ratio = h_out/Pt(h)
    return w_out, h_out, resize_ratio


def inch_to_pt(x, inverse=False):
    if inverse:
        return x/72
    return x*72


# gen_slide()
