import os
from os.path import join as pj
from sys import platform

CWD = os.getcwd()
deploy = True
debug = False
ICAN_IP='140.112.26.235'
GOOGLE_AUTH = pj(CWD, 'auth_info' , 'sinuous.json')
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = pj(
    CWD, GOOGLE_AUTH)

# db info
DB_USER = 'root'
DB_URL = '127.0.0.1:3306'
DB_NAME = 'cms'
DB_TABLE = 'component_dep'
DB_CMS_DIR = pj('cms-data')
DB_PWD = 'root'

# server info
if deploy:
    URL = ICAN_IP
else:
    URL = '127.0.0.1'

PORT_MAIN = '5001'
PORT_CMS = '5002'
PORT_SG = '5003'
PORT_UTIL = '5004'

HOST_MAIN = f'{URL}:{PORT_MAIN}'
HOST_CMS = f'{URL}:{PORT_CMS}'
HOST_SG = f'{URL}:{PORT_SG}'
HOST_UTIL = f'{URL}:{PORT_UTIL}'

CMS_DIR = pj('cms-data')
CMS_API_SG = f'http://{URL}:{PORT_CMS}/sg/cms'


SG_TESTCASE_DIR = pj( 'test-cases')
SG_DIR = pj(CWD, 'sg-data')
SG_SAMPLE_FILE1 = 'example-1-library.xml'
SG_SAMPLE_FILE2 = 'example-2-classroom.xml'
SG_SAMPLE_FILE3 = 'example-3-street.xml'

SG_UPLOAD_PATH = pj(CWD, 'sg-data', 'uploads')
SG_OUTPUT_PATH = pj(CWD, 'sg-data', 'outputs')

""" Ref for slide types: 
0 ->  title and subtitle
1 ->  title and content
2 ->  section header
3 ->  two content
4 ->  Comparison
5 ->  Title only 
6 ->  Blank
7 ->  Content with caption
8 ->  Pic with caption
"""
SG_CONTENT_LAYOUT = "Title only"
SG_ADD_FIRST_PAGE = False
SG_TMP_VOICE = pj(CWD, 'sg-data', 'tmp_voice')
SG_TMP_RHUBARB = pj(CWD , 'sg-data', 'tmp_rhubarb')
SG_TMP_LIPSYNCGIF = pj(CWD , 'sg-data' , 'tmp_lipsyncgif')
SG_TEST_MOUTH = pj(CWD , 'cms-data', 'component', 'mouth_set_1')
SG_RHUBARB_EXE = {
    'linux': 'rhubarb',
    'darwin': 'rhubarb',
    'win32': 'rhubarb.exe'
}.get(platform)
SG_RHUBARB_DIR = {
    #'linux': pj('rhubarb', 'Rhubarb-Lip-Sync-1.12.0-Linux'), # require glibc-2.29
    'linux': pj(CWD , 'rhubarb', 'Rhubarb-Lip-Sync-1.10.0-Linux'),
    'darwin': pj(CWD , 'rhubarb', 'Rhubarb-Lip-Sync-1.12.0-macOS'),
    'win32': pj(CWD , 'rhubarb', 'Rhubarb-Lip-Sync-1.12.0-Windows')
}.get(platform)

SG_BANNER_SRC = pj(CWD , 'sg-data' , 'banner.jpg')

# init tmp dir
if CWD.endswith('cms-integration'):
    for tmp in [SG_TMP_LIPSYNCGIF , SG_TMP_RHUBARB , SG_TMP_VOICE]:
        if not os.path.exists(tmp):
            os.mkdir(tmp)

def update_host():
    global HOST_MAIN
    global HOST_CMS
    global HOST_SG
    global HOST_UTIL

    global CMS_DIR
    global CMS_API_SG
    global debug


    HOST_MAIN = f'{URL}:{PORT_MAIN}'
    HOST_CMS = f'{URL}:{PORT_CMS}'
    HOST_SG = f'{URL}:{PORT_SG}'
    HOST_UTIL = f'{URL}:{PORT_UTIL}'

    CMS_API_SG = f'http://{URL}:{PORT_CMS}/sg/cms'

    
