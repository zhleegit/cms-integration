"""
致 接手這東西的小夥伴﹔


這玩意從 idea 到完成經過一年多風風雨雨，
滿是歷史的軌跡與忘記意義的 Dependencies
不建議在未知後果的情況下動它!
開發新功能建議用「疊加法」，
閒到不知道要幹麻才考慮 refactor or re-write
想清楚你要的是什麼

Remember three things:

- "Your time is limited, 
so don't waste it living someone else's life". - Steven Jobs

- Always save a runnable version!

- If it works, don't touch it!

"""


import os
import sys

from flask import (Flask, render_template, request,
                   send_from_directory)

import config

if 'test' in sys.argv[-1]:
    config.URL = '127.0.0.1'
    config.debug = True
    config.update_host()

app = Flask(__name__, static_folder='/')
PORT = config.PORT_MAIN

URL = config.URL


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

# render frontend packages
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@app.route('/assets/<path:path>')
def send_assets(path):
    return send_from_directory('static/assets', path)


@app.route("/", methods=["POST", "GET"])
def index():
    port_cms = config.PORT_CMS
    port_sg = config.PORT_SG
    url_cms = f'{URL}:{port_cms}'
    url_sg = f'{URL}:{port_sg}'
    return render_template("home.html",
                           data={'url_cms': url_cms,
                                 'url_sg': url_sg})


if __name__ == '__main__':

    debug = True if config.debug else  False 
    app.run(debug=debug, port=PORT, host='0.0.0.0')
