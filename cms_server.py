"""
致 接手這東西的小夥伴﹔


這玩意從 idea 到完成經過一年多風風雨雨，
滿是歷史的軌跡與忘記意義的 Dependencies
不建議在未知後果的情況下動它!
開發新功能建議用「疊加法」，
閒到不知道要幹麻才考慮 refactor or re-write
想清楚你要的是什麼

Remember three things:

- "Your time is limited, 
so don't waste it living someone else's life". - Steven Jobs

- Always save a runnable version!

- If it works, don't touch it!

"""

import os
import sys
# from copy import deepcopy
from glob import glob
from os.path import basename
from os.path import join
from os.path import join as pj

import numpy as np
from flask import (Flask, flash, jsonify, redirect, render_template, request,
                   send_from_directory, url_for)
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from pptx import Presentation
from sqlalchemy import and_, func, or_

import config
# import includes.LabelRecommender as LR
# import includes.slideHelper as SH
from utils.Logger import printLog

from sg_procedure import hash_filename as hash_for_dir

from utils.helper_word_embedding import WordEmbedding

QE = None

printLog('loading WordEmbedding')
WE = WordEmbedding()

if 'test' in sys.argv[-1]:
    config.URL = '127.0.0.1'
    config.debug = True
    config.update_host()


db_user = config.DB_USER
db_url = config.DB_URL
db_name = config.DB_NAME
db_pwd = config.DB_PWD
db_uri = f'mariadb+pymysql://{db_user}:{db_pwd}@{db_url}/{db_name}?charset=utf8'
db_cms_dir = config.CMS_DIR


app = Flask(__name__, static_folder='/')
CORS(app)
app.config['UPLOAD_SLIDE'] = join('uploads', 'slides')
app.config['UPLOAD_IMG'] = join('uploads', 'animations')
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = "Secret Key"
app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {
    "pool_pre_ping": True,
    "pool_recycle": 3600,
    'pool_timeout': 900,
    'pool_size': 10,
    'max_overflow': 5,
}


PORT = config.PORT_CMS
DB_TABLE = config.DB_TABLE
printLog(f'connected to DB_TABLE: {DB_TABLE}')

NEW_SLIDE_NAME = ""
TMP_SLIDE_PATH = join('tmp', 'tmp.pptx')
BACKGROUND_PATH = join('datasource', 'background')
PAGING_CHARACTERS = '\r\n'
top_n = 3
db = SQLAlchemy(app)


class ComponentTable(db.Model):
    __tablename__ = DB_TABLE
    id = db.Column(db.Integer, primary_key=True)
    src = db.Column(db.TEXT)
    tag1 = db.Column(db.TEXT)
    tag2 = db.Column(db.TEXT)
    tag3 = db.Column(db.TEXT)
    tag4 = db.Column(db.TEXT)
    tag5 = db.Column(db.TEXT)
    tag6 = db.Column(db.TEXT)
    tag7 = db.Column(db.TEXT)
    tag8 = db.Column(db.TEXT)

    def __init__(self, src,  tags):
        self.src = src
        for i in range(len(tags)):
            if tags[i] is np.nan:
                continue
            self.assign_tags(tags, i)

    def assign_tags(self, tags, i):
        if i == 0:
            self.tag1 = tags[i]
        elif i == 1:
            self.tag2 = tags[i]
        elif i == 2:
            self.tag3 = tags[i]
        elif i == 3:
            self.tag4 = tags[i]
        elif i == 4:
            self.tag5 = tags[i]
        elif i == 5:
            self.tag6 = tags[i]
        elif i == 6:
            self.tag7 = tags[i]
        elif i == 7:
            self.tag8 = tags[i]


Component = ComponentTable


@app.errorhandler(404)
def handle_404(e):
    return redirect(url_for('index'))


@app.route('/')
def index():
    return redirect('/cms')


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@app.route('/assets/<path:path>')
def send_assets(path):
    return send_from_directory('static/assets', path)


class UI_Component:
    def __init__(self, id, src,  tags, mouth=False) -> None:

        self.mouth = mouth
        self.id = id
        self.src = src
        self.name = basename(src)
        self.tags = tags

    def keep_non_na(self,  tags):
        return [t for t in tags if t != None]


def query_condition(keywords, logic='or'):
    """
    or: return list of cond that shall be or_
    and: return one cond that can be used directly
    """

    cond = []
    for keyword_raw in keywords:
        keyword = f"%{keyword_raw}%"

        tmp = [(Component.tag1.like(keyword)),
               (Component.tag2.like(keyword)),
            (Component.tag3.like(keyword)),
               (Component.tag4.like(keyword)),
               (Component.tag5.like(keyword)),
               (Component.tag6.like(keyword)),
               (Component.tag7.like(keyword)),
               (Component.tag8.like(keyword))]
        if logic == 'or':
            cond += and_(*tmp)
        else:
            cond += tmp

    if logic == 'and':
        cond = and_(*cond)

    return cond


def get_tag_cond(keyword_raw, to_cond=True):
    keyword = keyword_raw.strip()
    keyword = f'%{keyword}%'
    cond = [(Component.tag1.like(keyword)),
            (Component.tag2.like(keyword)),
            (Component.tag3.like(keyword)),
            (Component.tag4.like(keyword)),
            (Component.tag5.like(keyword)),
            (Component.tag6.like(keyword)),
            (Component.tag7.like(keyword)),
            (Component.tag8.like(keyword))]
    if not to_cond:
        return cond
    return or_(*cond)


def get_tag_cond_not(keyword_raw, to_cond=True):
    """

    """
    keyword = keyword_raw.strip()
    cond = [or_(Component.tag1 != keyword, Component.tag1 == None),
            or_(Component.tag2 != keyword, Component.tag2 == None),
            or_(Component.tag3 != keyword, Component.tag3 == None),
            or_(Component.tag4 != keyword, Component.tag4 == None),
            or_(Component.tag5 != keyword, Component.tag5 == None),
            or_(Component.tag6 != keyword, Component.tag6 == None),
            or_(Component.tag7 != keyword, Component.tag7 == None),
            or_(Component.tag8 != keyword, Component.tag8 == None)]
    return and_(*cond)


def get_cond(input_cond):

    if ' ' not in input_cond:
        return get_tag_cond(input_cond)
    elif (' OR ' in input_cond) or (' ' in input_cond.replace(' AND ', '')):
        and_statements = input_cond.strip()\
            .replace(' OR ', '[SEP]')\
            .replace(' AND ', '[AND]')\
            .replace(' ', '[SEP]')\
            .replace('[AND]', ' AND ')\
            .split('[SEP]')

        and_conds = []
        for statement in and_statements:
            and_conds.append(get_cond(statement))
        return or_(*and_conds)

    elif ' AND ' in input_cond and (' OR ' not in input_cond or ' ' not in input_cond.replace(' AND ', '')):
        and_keywords = input_cond.split(' AND ')
        and_conds = []
        for keyword in and_keywords:
            and_conds.append(get_cond(keyword))
        return and_(*and_conds)


def query_component(searchtext, mouth=False):
    if mouth:
        query = Component.query.filter(get_cond('mouth'))
    else:
        query = Component.query.filter(get_tag_cond_not('mouth'))

    if type(searchtext) is not str or \
            searchtext.strip() == '':
        printLog('query for all')
        return [res for res in query.all() if res is not None]
    query_res = query.filter(get_cond(searchtext.strip())).all()

    return [res for res in query_res if res is not None]


@app.route('/api/search')
def api_search():
    """
    query text by api
    """
    searchtext = request.args.get('searchtext', '', type=str)
    res = query_component(searchtext)
    printLog('query res: len ' + str(len(res)))

    img_list = [UI_Component(comp.id, comp.src, [comp.tag1,
                                                 comp.tag2,
                                                 comp.tag3,
                                                 comp.tag4,
                                                 comp.tag5,
                                                 comp.tag6,
                                                 comp.tag7,
                                                 comp.tag8]).__dict__ for comp in res]
    return jsonify({'keyword': searchtext, 'data': img_list})


# cms is used to replace 'manage_animaiton'
@app.route("/cms", methods=["GET", "POST"])
def cms():
    """
    query text by calling funciton
    """
    # if request.method == 'GET':
    search_text = request.args.get('searchtext', '').lower().strip()


    use_sim = request.args.get('use_sim', 'NOT receive from frontend')
    printLog(f'receive use_sim: {use_sim}')

    if use_sim == 'true' and len(search_text) > 0:

        sim_search_text = search_text
        if ' ' in sim_search_text:
            sim_search_text = sim_search_text.split(' ')[0]

        all_tags = get_all_tags_list()
        print(all_tags)
        rank_list = WE.get_rank_list(sim_search_text, all_tags)
        rank_list = [tag for tag, score in rank_list]

        for n in range(top_n, 1-1, -1):
            if len(rank_list) >= n:
                rank_list = rank_list[:n]
                break

        search_text = ' OR '.join(rank_list)
        print(rank_list)

        printLog(f'search_text for sim: {search_text}')
        res = query_component(search_text)
        res_len = len(res)
        printLog('query res: len ' + str(res_len))

        if res_len == 0:
            res = query_component(None)
            search_text = '[沒有符合條件的結果，顯示所有元件]'

        def get_represent_tag(x_list):
            print('compare rank_list with x_list')
            print('rank_list: ' , rank_list)
            print('x_list: ' , x_list)

            rtags = list(set(rank_list) & set(x_list))
            rtags = sorted(rtags, key=lambda rtag: -WE.sim_of_two_text(
                                                        rtag,
                                                        sim_search_text))
            return rtags[0]

        img_list = [UI_Component(comp.id, comp.src, [comp.tag1,
                                                     comp.tag2,
                                                     comp.tag3,
                                                     comp.tag4,
                                                     comp.tag5,
                                                     comp.tag6,
                                                     comp.tag7,
                                                     comp.tag8]) for comp in res]
        if 'male' in search_text.lower() and 'female' not in search_text.lower():
            img_list = list(filter(lambda x: 'female' not in x.tags, img_list))

        # feat: sims list
        img_list_with_score = [[obj,
                                get_represent_tag(obj.tags),
                                WE.sim_of_two_text(
                                    get_represent_tag(obj.tags), sim_search_text)]
                               for obj in img_list if len(list(set(rank_list) & set(obj.tags)))>0]
        img_list_with_score = sorted(img_list_with_score, key=lambda x: -x[-1])

        # divide img_list_with_score to img_list and sims
        img_list = [with_score[0] for with_score in img_list_with_score]
        sims = [[with_score[1], str(with_score[-1])]
                for with_score in img_list_with_score]

        search_text = f'與 {sim_search_text} 最相似標籤'

        return render_template('CMS.html', imgs=img_list,
                               search_text=search_text,
                               main_url=f'http://{config.HOST_MAIN}',
                               sims=sims,
                               sim_search_text=sim_search_text)

    else:

        printLog(f'search_text: {search_text}')
        res = query_component(search_text)
        res_len = len(res)
        printLog('query res: len ' + str(res_len))
        if res_len == 0:
            res = query_component(None)
            search_text = '[沒有符合條件的結果，顯示所有元件]'

        img_list = [UI_Component(comp.id, comp.src, [comp.tag1,
                                                     comp.tag2,
                                                     comp.tag3,
                                                     comp.tag4,
                                                     comp.tag5,
                                                     comp.tag6,
                                                     comp.tag7,
                                                     comp.tag8]) for comp in res]
        if 'male' in search_text.lower() and 'female' not in search_text.lower():
            img_list = list(filter(lambda x: 'female' not in x.tags, img_list))

        sims = None
        return render_template('CMS.html', imgs=img_list,
                               search_text=search_text,
                               main_url=f'http://{config.HOST_MAIN}',
                               sims=sims)


def hash_filename(fn):
    new_fn = hash_for_dir(fn)
    return new_fn


@app.route('/insert', methods=['POST'])
def insert():
    if request.method == 'POST' \
            and request.files.get('component-file') \
            and all([request.form.get(f'tag{i+1}') is not None for i in range(8)]):

        file_obj = request.files.get('component-file')
        filename = file_obj.filename
        tags = [request.form.get(f'tag{i+1}') for i in range(8)]
        tags = sorted(list(set([t if t != '' else None for t in tags])),
                      key=lambda x: -ord(x[0]) if x is not None else -1)
        while len(tags) < 8:
            tags.append(None)

        # save file
        while os.path.exists(pj(db_cms_dir, 'component', hash_filename(filename))):
            filename += 'x'
        filename = hash_filename(filename)
        src = pj(db_cms_dir, 'component', filename)
        file_obj.save(src)

        # save record
        new_record = Component(src, tags)
        db.session.add(new_record)
        db.session.commit()

        flash("Component Inserted Successfully")
    else:
        flash("No file inserted")
    return redirect(url_for('index'))


# this is our update route where we are going to update our employee
@app.route('/update', methods=['GET', 'POST'])
def update():

    if request.method == 'POST':
        tags = [request.form.get(f'tag{i+1}') for i in range(8)]
        tags = sorted(list(set([t if t != '' else None for t in tags])),
                      key=lambda x: -ord(x[0]) if x is not None else -1)
        while len(tags) < 8:
            tags.append(None)

        my_data = Component.query.get(request.form.get('id'))
        for i in range(8):
            my_data.assign_tags(tags, i)

        db.session.commit()
        flash("Component Updated Successfully")

        return redirect(url_for('index'))


# This route is for deleting our employee
@app.route('/delete/<id>/', methods=['GET', 'POST'])
def delete(id):
    # todo: mouth shall be
    my_data = Component.query.get(id)

    if not os.path.exists(my_data.src):
        flash('File source does not exist!')
        redirect(url_for('index'))
    try:
        os.remove(my_data.src)
        printLog(f'delete file from {my_data.src}')
    except:
        flash('Fail to remove file! Please check whether it is openned!')
        redirect(url_for('index'))

    db.session.delete(my_data)
    db.session.commit()
    flash("Component Deleted Successfully")

    return redirect(url_for('index'))


@app.route("/manage_slide", methods=["POST", "GET"])
def manage_slide():
    slide_list = glob(join(app.config['UPLOAD_SLIDE'], '*'))
    slide_list.sort()
    for i in range(len(slide_list)):
        slide_list[i] = [basename(slide_list[i]), slide_list[i]]
    return render_template('ManageSlide.html', slides=slide_list)


@app.route("/manage_slide/deal_with_slides", methods=['POST'])
def deal_with_slide():
    if request.method == 'POST':
        if request.form.get('back'):
            return redirect('/')

        elif request.form.get('upload') and request.files['slide_file']:
            slide_file = request.files['slide_file']
            slide_name = check_slide_name(slide_file.filename)
            slide_path = join(app.config['UPLOAD_SLIDE'], slide_name)
            prs = Presentation(slide_file)
            prs.save(slide_path)

        elif request.form.get('download'):
            slide_path = request.form.get('download')
            return send_from_directory(app.config['UPLOAD_SLIDE'], basename(slide_path), as_attachment=True)

        elif request.form.get('add'):
            global NEW_SLIDE_NAME
            slide_path = request.form.get('add')
            prs = Presentation(slide_path)
            prs.save(TMP_SLIDE_PATH)
            NEW_SLIDE_NAME = check_slide_name(basename(slide_path))
            return add_animation_to_slide()

        elif request.form.get('delete'):
            slide_path = request.form.get('delete')
            os.remove(slide_path)

    return redirect('/manage_slide')


def add_animation_to_slide():
    global ADD_LIST
    prs = Presentation(TMP_SLIDE_PATH)
    ADD_LIST = [[] for i in range(len(prs.slides))]
    return choose_animation(0)


def choose_animation(page):
    prs = Presentation(TMP_SLIDE_PATH)
    if page < 0:
        page = 0
    elif page >= len(prs.slides):
        page = len(prs.slides)-1

    global ADD_LIST
    ADD_LIST[page] = []

    text_list = SH.pptx_to_text_list(prs, page=page)
    texts = SH.get_slide_text(page, prs.slides[page])
    imgs = QE.runtime_get_topk_pairs_from_multi_sentences(texts)
    return render_template('ChooseAnimation.html',
                           text=text_list,
                           newname=NEW_SLIDE_NAME,
                           page=page,
                           img_src=imgs)


def check_slide_name(filename, n=0):
    file_list = os.listdir(app.config['UPLOAD_SLIDE'])
    if n != 0:
        new_fn = filename.split(
            '.')[0] + '_{}.'.format(n) + filename.split('.')[1]  # a.pptx >> a_1.pptx
    else:
        new_fn = filename
    for i in range(len(file_list)):
        if file_list[i] == new_fn:
            return check_slide_name(filename, n+1)
    return new_fn


# cms is used to replace 'manage_animaiton'
@app.route("/sg/cms", methods=["GET", "POST"])
def cms_sg():
    """
    query text by calling funciton
    non mouth
    """
    if request.method == 'GET':
        search_text = request.args.get('searchtext', '')
        sg_config_id = request.args.get('sg_config_id', 'NULL_SG_ID')
    else:
        search_text = ''
        sg_config_id = 'NULL_SG_ID'
    if search_text.strip() != '':
        res = query_component(search_text)
    else:
        res = query_component(None)
    res_len = len(res)
    printLog(f'search_text: {search_text}')
    printLog('query res: len ' + str(res_len))
    if res_len == 0:
        res = query_component(None)
        search_text = '[沒有符合條件的結果，顯示所有元件]'

    img_list = [UI_Component(comp.id, comp.src, [comp.tag1,
                                                 comp.tag2,
                                                 comp.tag3,
                                                 comp.tag4,
                                                 comp.tag5,
                                                 comp.tag6,
                                                 comp.tag7,
                                                 comp.tag8]) for comp in res]
    if 'male' in search_text.lower() and 'female' not in search_text.lower():
        img_list = list(filter(lambda x: 'female' not in x.tags, img_list))
    return render_template('CMS_sg.html',
                           imgs=img_list,
                           search_text=search_text,
                           sg_config_id=sg_config_id)



@app.route("/sg/cms_mouth", methods=["GET", "POST"])
def cms_sg_mouth():
    """
    query text by calling funciton
    mouth
    """
    if request.method == 'GET':
        search_text = request.args.get('searchtext', '')
        sg_config_id = request.args.get('sg_config_id', 'NULL_SG_ID')
    else:
        search_text = ''
        sg_config_id = 'NULL_SG_ID'

    if search_text.strip() != '':
        res = query_component(search_text, mouth=True)
    else:
        res = query_component(None, mouth=True)
    printLog(f'search_text: {search_text}')
    printLog('query res: len ' + str(len(res)))

    img_list = [UI_Component(comp.id, comp.src, [comp.tag1,
                                                 comp.tag2,
                                                 comp.tag3,
                                                 comp.tag4,
                                                 comp.tag5,
                                                 comp.tag6,
                                                 comp.tag7,
                                                 comp.tag8], mouth=True) for comp in res]
    return render_template('CMS_mouth.html',
                           imgs=img_list,
                           search_text=search_text,
                           sg_config_id=sg_config_id)


def get_all_tags_list():
    res = query_component(None)

    if len(res) == 0:
        return
    all_tags = []
    for comp in res:
        all_tags += [comp.tag1,
                     comp.tag2,
                     comp.tag3,
                     comp.tag4,
                     comp.tag5,
                     comp.tag6,
                     comp.tag7,
                     comp.tag8]
    all_tags = list(set(all_tags))
    return list(filter(lambda x: x is not None, all_tags))


def init_WE_with_all_tags():
    global WE
    all_tags = get_all_tags_list()
    printLog(f'init WE with {len(all_tags)} tags')
    WE.init_dict(all_tags)


if __name__ == '__main__':
    debug = True if config.debug else False
    init_WE_with_all_tags()
    app.run(debug=debug, port=PORT, host='0.0.0.0')
