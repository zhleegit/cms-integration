import cv2
import random
import os
from os import listdir, chdir, system
from glob import glob 
from PIL import Image
from google.cloud import texttospeech

MODEL_SOURCE = os.path.join('datasource', 'cascade.xml')
VOICE_NAME = 'en-US-Standard-H' #MALE: ABDIJ FEMALE: CEFGH

RHUBARB_DIR = 'rhubarb'
RHUBARB_DIR_TO_ORIGIN = '..'
RHUBARB_EXE = './rhubarb'
#RHUBARB_EXE = 'rhubarb.exe'

def random_choose_png(src_path):
    src_list = glob(os.path.join(src_path, '*.png'))
    i = random.randrange(0, len(src_list), 1)
    return src_list[i]


def random_choose_dir(src_path):
    src_list = glob(os.path.join(src_path, '*'))
    i = random.randrange(0, len(src_list), 1)
    return src_list[i]


def text_to_speech(text_in, wav_path):
    """
    Synthesizes speech from the input string of text or ssml.
    Note: ssml must be well-formed according to:
        https://www.w3.org/TR/speech-synthesis/
    """
    # Instantiates a client
    client = texttospeech.TextToSpeechClient()
    # Set the text input to be synthesized
    synthesis_input = texttospeech.SynthesisInput(text=text_in)
    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.VoiceSelectionParams(
        language_code="en-US", name = VOICE_NAME, ssml_gender=texttospeech.SsmlVoiceGender.NEUTRAL
    )
    # Select the type of audio file you want returned
    audio_config = texttospeech.AudioConfig(
        audio_encoding=texttospeech.AudioEncoding.LINEAR16
    )
    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(
        input=synthesis_input, voice=voice, audio_config=audio_config
    )
    # The response's audio_content is binary.
    with open(wav_path, "wb") as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('\nAudio content written to file "'+ wav_path + '"\n')


def detect(image):
    detector = cv2.CascadeClassifier(MODEL_SOURCE)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    mins = (image.shape[0] // 120) * 10
    maxs = (image.shape[0] //40) * 10
    smiles = detector.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=50, minSize=(mins, mins), maxSize=(maxs, maxs))
    
    # ouput the image with mouth rectangle
    if len(smiles) <= 0:
        print("Mouth detect fail.")
        return

    return smiles


def change_mouth(image, smiles, mouth_path, result_path):
    mouth_list = listdir(mouth_path)
    #find best in smiles
    miny = 10000
    for i in smiles:
        if i[1] < miny:
            (x,y,w,h) = i
            miny = y
    y = int(y + h/5)
    r = int((h+1)/2*2)
    x = int(x + w/2 - r/2)

    for mouth_fn in mouth_list:
        mouth = cv2.imread(os.path.join(mouth_path, mouth_fn), cv2.IMREAD_UNCHANGED)
        temp = image.copy()
        
        mouth = cv2.resize(mouth, (r, r), cv2.COLOR_BGR2GRAY)

        for i in range(int(mouth.shape[0]*0.7)):
            for j in range(mouth.shape[1]):
                if mouth[i][j][3] != 0:
                    for k in range(3):
                        temp[y+i][x+j][k] = mouth[i][j][k]

        cv2.imwrite(os.path.join(result_path, mouth_fn), temp)


def random_replace_mouth(img_src, mouth_src, result_path):
    img_path = random_choose_png(img_src)
    img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
    smiles = detect(img.copy())
    mouth_path = random_choose_dir(mouth_src)
    print(img_path, mouth_path)
    change_mouth(img, smiles, mouth_path, result_path)


def make_lip_sync_gif(src_path, gif_path, mouth_path, dur=0.01):
    # Read PNG
    src_img_fn = ['A.png', 'B.png', 'C.png', 'D.png', 'E.png', 'F.png', 'G.png', 'H.png']
    src_img = []
    mouth_list = []
    gif_list = []

    for fn in src_img_fn:
        img = Image.open(os.path.join(src_path, fn))
        #deal with transparency
        alpha = img.getchannel('A')
        img = img.convert('RGB').convert('P', palette=Image.ADAPTIVE, colors=255)
        mask = Image.eval(alpha, lambda a: 255 if a <=128 else 0)
        img.paste(255, mask)
        img.info['transparency'] = 255
        src_img.append([fn[0], img])

    # Read mouth.txt
    with open(mouth_path) as mouth_f:
        for line in mouth_f:
            mouth = line[:-1].split('\t')
            if mouth[1] == 'X':
                mouth[1] = 'A'
            mouth_list.append([mouth[0], mouth[1]])
        for i in range(0, len(mouth_list)-1):
            mouth_list[i][0] = float(mouth_list[i+1][0]) - float(mouth_list[i][0])
        mouth_list.pop(len(mouth_list)-1)

    # Generate gif list
    for i in mouth_list:
        # Round the duration
        i[0] = int((float(i[0]) + dur/2)//dur)
        for img in src_img:
            if i[1] == img[0]:
                for j in range(0, i[0]):
                    gif_list.append(img[1])

    gif_list[0].save(gif_path, save_all=True,
                        append_images=gif_list[1:], duration=dur*1000, loop=1)
    print('\nGif is written to file "' + gif_path + '"')

def text_to_animation(text, wav_fn, gif_fn, result_path):
    wav_path = os.path.join(result_path, wav_fn)
    gif_path = os.path.join(result_path, gif_fn)
    mouth_path = os.path.join(result_path, 'mouth.txt')
    text_to_speech(text, wav_path)
    chdir(RHUBARB_DIR)
    system('{} {} -o {}'.format(RHUBARB_EXE,
                                os.path.join(RHUBARB_DIR_TO_ORIGIN, wav_path),
                                os.path.join(RHUBARB_DIR_TO_ORIGIN, mouth_path)))
    chdir(RHUBARB_DIR_TO_ORIGIN)
    make_lip_sync_gif(result_path, gif_path, '{}/mouth.txt'.format(result_path))
