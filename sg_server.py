import os
import sys
# import threading
import traceback
# from glob import glob
from os.path import basename
from os.path import join as pj
# from pprint import pprint

import requests
from flask import (Flask, flash, jsonify, redirect, render_template, request,
                   send_file, send_from_directory, url_for)
# from numpy import numarray
# from pptx import Presentation

import config
from sg_procedure import Slide, gen_slide
from utils.Logger import printLog

"""
    userConfig: 
        - init slides param (util_server xml parsing tool)
        - bargine param with user
        - submit json form

    submit_json_param:
        - slides param json form -> slide_obj form
        - call sg_procesure.gen_slide(slides)
    
    """

if 'test' in sys.argv[-1]:
    config.URL = '127.0.0.1'
    config.debug = True
    config.update_host()


app = Flask(__name__, static_folder='/')
PORT = config.PORT_SG
SG_DIR = config.SG_DIR
UPLOAD_PATH = config.SG_UPLOAD_PATH
CMS_API_SG = config.CMS_API_SG


app.secret_key = "Secret Key"

# render frontend packages
from sg_procedure import hash_filename as hash_for_dir


def hash_filename(fn, target_folder = '.'):
    new_fn = hash_for_dir(fn, target_folder=target_folder)
    return new_fn


@app.errorhandler(404)
def handle_404(e):
    return redirect(url_for('index'))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@app.route('/assets/<path:path>')
def send_assets(path):
    return send_from_directory('static/assets', path)


@app.route('/')
def root():
    return redirect('/index')


@app.route('/index')
def index():
    msg = request.args.get('err_msg', None)
    output_path = request.args.get('output_path', None)

    if output_path is not None:
        output_fn = basename(output_path)
    else:
        output_fn = None

    if msg is not None:
        flash(msg)

    return render_template('slide_generation.html',
                           main_url=f'http://{config.HOST_MAIN}',
                           output_fn=output_fn)


@app.route('/download_sample1')
def download_sample1():
    return send_file(pj(config.SG_TESTCASE_DIR, config.SG_SAMPLE_FILE1), as_attachment=True)


@app.route('/download_sample2')
def download_sample2():
    return send_file(pj(config.SG_TESTCASE_DIR, config.SG_SAMPLE_FILE2), as_attachment=True)


@app.route('/download_sample3')
def download_sample3():
    return send_file(pj(config.SG_TESTCASE_DIR, config.SG_SAMPLE_FILE3), as_attachment=True)


@app.route('/upload_script', methods=['POST', 'GET'])
def upload_script():
    if request.method == 'POST' and request.files.get('xml_file'):
        file_obj = request.files['xml_file']
        file_name = file_obj.filename

        new_filename = hash_filename(file_name, UPLOAD_PATH)
        file_src = pj(UPLOAD_PATH, new_filename)
        file_obj.save(file_src)

        return redirect(url_for('check_script', file_src=file_src))
        # return redirect(url_for('generation_procedure', file_src=file_src))

    return redirect(url_for('index', err_msg='invalid upload method'))


@app.route('/check_script')
def check_script():
    file_src = request.args.get('file_src', '', type=str)

    if not os.path.exists(file_src):
        redirect(url_for('index', err_msg='inbalid xml path'))

    file_content = ''
    with open(file_src, encoding='utf-8') as f:
        file_content = f.read()

    return render_template('check_script.html',
                           data_content=file_content,
                           file_src=file_src)

    # return redirect(url_for('generation_procedure', file_src=file_src))


# (/generation_procedure and /submit_json_param)
# config type:
#
# config inferenced from xml
# config interacted with user


class SG_Collection:

    def __init__(self) -> None:
        self.collection = {}
        self.progress_str_config = '5'

    def store_slides(self, key, slides):
        if key not in self.collection:
            self.collection[key] = slides
            return True
        else:
            printLog(f'duplicate key: {key} in SG_collection')
            return False

    def get_slides(self, key):
        return self.collection.get(key)

    def set_slide_srcs(self,
                       key: str,
                       actor_srcs: list,
                       mouth_dirs: list,
                       bg_src: str):

        slides = self.get_slides(key)
        if slides is None:
            printLog(f"key: {key} is invalid")
            return None

        for i in range(len(slides)):
            slides[i].actor_srcs = actor_srcs
            slides[i].background_src = bg_src
            slides[i].mouth_dirs = mouth_dirs

    def flush_slides(self, key):
        if key not in self.collection:
            printLog(f"flushed key : {key} is not in collection")
            return None
        return self.collection.pop(key)


sg_collection = SG_Collection()




@app.route('/generation_procedure')
def generation_procedure():
    # sem.acquire()
    global sg_collection
    global percentage_str
    percentage_str = '0'
    sg_collection.progress_str_config = '0'
    scene, scene_raw, actors, gender_query, file_src = None, None, None, None, None
    err_msg = ''

    try:
        # clean tmp lipsync gif
        # old_gif = [r for r in os.listdir(
        #     config.SG_TMP_LIPSYNCGIF) if r.lower().endswith('gif')]
        # if len(old_gif) != 0:
        #     for old_fn in old_gif:
        #         os.remove(pj(config.SG_TMP_LIPSYNCGIF, old_fn))

        # # clean tmp rhubarb output
        # old_rhu = [r for r in os.listdir(
        #     config.SG_TMP_RHUBARB) if r.lower().startswith('rhu')]
        # if len(old_rhu) != 0:
        #     for old_fn in old_rhu:
        #         os.remove(pj(config.SG_TMP_RHUBARB, old_fn))

        file_src = request.args.get('file_src', '', type=str)
        if file_src == '':
            err_msg = 'file source does not exist'
            assert False, err_msg
            # redirect(url_for('index',  err_msg=err_msg))
        printLog(f'request from util host: {config.HOST_UTIL}')
        r = requests.get(f'http://{config.HOST_UTIL}/read_xml',
                         params={'xml_filename': file_src})
        xml_data = r.json()

        # pprint(xml_data)
        res = xml_data.get('res')
        if res is None:
            err_msg = 'res not found in /generation_procedure'

        if not res.get('success', False):  # todo : error
            err_msg = 'something wrong when parsing xml file'

        # solve speaker and actor_srcs
        actor_to_idx = res.get('actors')
        actor_set_lower = [str(actor).lower() for actor in actor_to_idx.keys()]
        if actor_to_idx is None:
            err_msg = 'actor to idx not found in /generation_procedure'
        # keep order of actors from json result

        if err_msg != '':
            assert False, err_msg
            # redirect(url_for('index', err_msg=err_msg))
        actor_array = sorted(
            [[k, v] for k, v in actor_to_idx.items()], key=lambda x: x[-1])
        actor_array = [actor_name for actor_name, _ in actor_array]

        # redirect(url_for('index', err_msg=err_msg))
        # todo: query actors
        actors = [[f'actor{i}', actor_array[i]]
                  for i in range(len(actor_array))]

        # solve background
        scene = res.get('scenes')
        scene_raw = scene
        scene_nouns = requests.get(f'http://{config.HOST_UTIL}/get_nouns',
                                   params={'sentence': scene}).json().get('nouns')
        scene_nouns = [noun for noun in scene_nouns if noun.lower()
                       not in actor_set_lower]
        if scene_nouns is None:
            err_msg = 'No noun found in scene description {scene}'
            assert False, err_msg
            # redirect(url_for('index', err_msg))

        printLog('scene_nouns')
        print(scene_nouns)
        scene = ' OR '.join(scene_nouns)

        slide_info = res.get('slides')
        slide_info = sorted(slide_info, key=lambda x: x['page_id'])

        slides = []
        total = len(slide_info)

        for i, d in enumerate(slide_info):
            percentage_str = str(int(100*(i+1)/total))

            slides.append(
                Slide(page_id=d['page_id'],
                      text=d['dialogue'],
                      actor_srcs=None,
                      speaker_name=actor_array[d['speaker_id']],
                      background_src=None,
                      speaker_gender=d['speaker_gender'].get('gender', 'f'),
                      speaker_id=d['speaker_id'])
            )
        gender_query = ['female' if d['speaker_gender'].get(
            'gender', 'f') == 'f' else 'male' for d in slide_info]

        sg_collection.flush_slides(key=file_src)
        assert sg_collection.store_slides(
            key=file_src, slides=slides), 'storage has problem'

    except:
        printLog("except occur")
        print(traceback.format_exc())

        if err_msg == '':
            err_msg = ' unexpected exception happens'

        # sem.release()
        return redirect(url_for('index', err_msg=err_msg))

    # sem.release()

    printLog(f"UserConfig with param:\n " +
             f"\ncms_api_sg={CMS_API_SG}," +
             f"\nscene={scene}," +
             f"\nscene_raw={scene_raw}," +
             f"\nactors={actors}," +
             f"\ngender_query={gender_query}," +
             f"\nkey={file_src}""")

    return render_template('UserConfig.html',
                           cms_api_sg=CMS_API_SG,
                           scene=scene,
                           scene_raw=scene_raw,
                           actors=actors,
                           gender_query=gender_query,
                           key=file_src)


cache = {}


def append_gender_to_name(name):
    global cache
    if name.lower() in cache:
        return cache[name.lower()]

    r = request.get(f'http://{config.HOST_UTIL}/get_gender',
                    params={'sentence': name}).json()
    gender = r.get('gender', 'f')
    prob = float(r.get('prob', 0))
    if prob < .6:
        return name

    gender = 'female' if gender == 'f' else 'male'
    result = f"{name} AND {gender}"
    cache[name.lower()] = result
    return result


@app.route('/submit_json_param', methods=['GET', 'POST'])
def submit_json_param():
    global sg_collection

    userConfig = request.form
    num_actor = int(userConfig.get('num_actor'))
    background_src = userConfig.get('scene')
    collection_key = userConfig.get('key')
    printLog(f'receive collection key : {collection_key}')
    actor_srcs = []
    mouth_dirs = []

    for i in range(num_actor):
        key = f'actor{i}'
        actor_srcs.append(userConfig.get(key))

        key = f'actor{i}_mouth'
        mouth_dirs.append(userConfig.get(key))

    sg_collection.set_slide_srcs(
        key=collection_key,
        actor_srcs=actor_srcs,
        mouth_dirs=mouth_dirs,
        bg_src=background_src)

    output_path = gen_slide(sg_collection.get_slides(collection_key),
                            out_fn=hash_filename('output.pptx',
                                                 target_folder=config.SG_OUTPUT_PATH
                                                 ),
                            sg_collection=sg_collection
                            )

    printLog(f'output path: {output_path}')

    # send_from_directory(output_path)

    return redirect(url_for('index', output_path=output_path))
    # return redirect("/index")


@app.route('/send')
def send_file_output():
    out_fn = request.args.get('out_fn', '', type=str)
    return send_from_directory(config.SG_OUTPUT_PATH, out_fn, as_attachment=True)


@app.route('/test')
def test():
    return render_template('/test.html')


@app.route('/clean')
def clean_upload():
    """
    /clean?pwd=zhlee
    """
    pwd = request.args.get('pwd', '', type=str)
    if pwd!='zhlee':
        return redirect(url_for('index', err_msg='invalid password'))
    
    folders = [config.SG_UPLOAD_PATH, config.SG_OUTPUT_PATH,
               config.SG_TMP_LIPSYNCGIF, config.SG_TMP_RHUBARB, config.SG_TMP_VOICE]
    for folder in folders:
        for fn in os.listdir(folder):
            fpath = pj(folder ,fn)
            os.remove(fpath)
    printLog("\n\n[WARNING]\n\nClean is operated")
    return redirect(url_for('index', err_msg='tmp, upload, output are cleaned'))


percentage_str = '0'


@app.route('/progress_sure', methods=['POST', 'GET'])
def progress():
    global percentage_str
    return jsonify({"percentage": percentage_str})


@app.route('/progress_sure_config', methods=['POST', 'GET'])
def progress_config():
    global sg_collection
    percentage_str_config = sg_collection.progress_str_config
    return jsonify({"percentage": percentage_str_config})


if __name__ == '__main__':
    debug = True if config.debug else  False 
    app.run(debug=debug, port=PORT, host='0.0.0.0')
